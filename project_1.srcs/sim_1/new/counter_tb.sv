//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/15/2020 12:45:38 PM
// Design Name: 
// Module Name: counter_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter_tb;
   // Time scale
   timeunit 1ns;
   timeprecision 100ps;

   // Counter parametrization value
   localparam WIDTH = 5;

   // TB signals
   logic [WIDTH-1:0]  data_i;
   logic 			  load_i; 
   logic 			  en_i;
   logic 			  clk_i = 1'b1;
   logic 			  rstn_i = 1'b1;
   logic [WIDTH-1:0]  count_o;

   // Clock period constant
`define PERIOD 10 				

   // Clock process
   always	
     #(`PERIOD/2) clk_i = ~clk_i;

   // Counter instance
   counter #(WIDTH) counter_5_inst
	 (
	  // Port mapping to TB signals
	  .data_i(data_i),
	  .load_i(load_i),
	  .en_i(en_i),   
	  .clk_i(clk_i), 
	  .rstn_i(rstn_i),
	  .count_o(count_o)
	  );

   // Signal monitor
   initial begin
	  $timeformat ( -9, 0, "ns", 3 ) ;
	  $monitor ( "%t data_i=0x%h count_o=0x%h en_i=%b rstn_i=%b load_i=%b",
				 $time,   data_i, count_o, en_i, rstn_i, load_i) ;
	  #(`PERIOD * 99)			// wait for 99 * 10 ns
	  $display ( "TEST TIMEOUT" );
	  $finish;					// Stops testbench
   end

   // Task to Verify Results
   task xpect (input [WIDTH-1:0] out_o, input [WIDTH-1:0] expects) ;
   	  if ( out_o !== expects ) begin
   		 $display ( "Output is %d but was expected to be %d", out_o, expects ) ;
   		 $display ( "TEST FAILED" );
   		 $finish;
   	  end
   endtask

   // Actual testbench
   initial begin
	  // Initialize signals
	  @(posedge clk_i)
		begin
		   rstn_i = 1'b1;
		   en_i = 1'bX;
		   load_i = 5'bXXXXX;
		   data_i = 5'bXXXXX;
		end

	  // Set reset to 0, should reset count_o
	  @(posedge clk_i)
		begin
		   rstn_i = 1'b0;		// Reset counter
		end
	  
	  @(negedge clk_i)
	  	xpect(count_o, 5'd0);	// Expect to find 0 at count_o

	  // Set normal operation
	  @(posedge clk_i)
		begin
		   rstn_i = 1'b1;		// Reset = 1
		   en_i = 1'b1;			// Counting enabled
		   load_i = 1'b0;		// Load is disabled
		end
	  
	  // On next positive edge, output should be 1
	  // Since counter is enabled and 1 period has passed
	  @(posedge clk_i)
	  	xpect(count_o, 5'd1);

	  // On next positive edge, output should be 2
	  @(posedge clk_i)
	  	xpect(count_o, 5'd2);

	  #(`PERIOD * 30)			// Wait for 30 clk periods
	  @(posedge clk_i)
		begin
	  	   xpect(count_o, 5'd0); // Wait for overflow, check that output is 0
		   data_i = 5'd31; // Try to write value 31 without load_i enabled
		end

	  @(posedge clk_i)
		begin
		   xpect(count_o, 5'b1); // On next edge, should be 1, not 31
		   en_i = 1'b0;	// Disable en_i so that we can write a new value
		   load_i = 1'b1;		// Data is still 31, enable write
		end

	  @(posedge clk_i)
		begin
		   xpect(count_o, 5'd31); // On next edge, count_o should have value of data_i
		end	  

	  $display ("ALL TESTS PASSED");
	  $finish(0);
   end

   
   
endmodule
