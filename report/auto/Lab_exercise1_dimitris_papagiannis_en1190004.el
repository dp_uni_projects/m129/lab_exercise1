(TeX-add-style-hook
 "Lab_exercise1_dimitris_papagiannis_en1190004"
 (lambda ()
   (setq TeX-command-extra-options
         "-output-directory=build")
   (TeX-run-style-hooks
    "uoa_template")
   (TeX-add-symbols
    "documentTitle")
   (LaTeX-add-labels
    "fig:counter_sketch"
    "fig:counter_schem_01"
    "fig:counter_schem_02"
    "fig:tb_reset"
    "fig:tb_count"
    "fig:tb_over"
    "fig:tb_load"
    "fig:tb_cu"))
 :latex)

